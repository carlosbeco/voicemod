import { UserModel } from '../models/User';
import AuthenticationService from '../service/AuthenticationService';
import EmailService from '../service/EmailService';
const { check, validationResult, body } = require('express-validator');
const bcrypt = require('bcrypt');

export default (app) => {
  app.post('/v1/auth', async (req, res) => {
    if (req.user) {
      //res.send(req.user).end();
      res.status(200).end();
    } else {
      res.status(401).end();
    }
  });

  app.post('/v1/register', [
    body('email').isEmail().normalizeEmail(),
    body('contrasena').isLength({min:6}),
    body('nombre').not().isEmpty().trim().escape(),
    body('apellidos').not().isEmpty().trim().escape(),
    body('pais').not().isEmpty().trim().escape(),
    body('telefono').not().isEmpty().trim().escape(),
    body('codigo_postal').not().isEmpty().isLength({min:5, max:5}).trim().escape(),
  ], async (req, res) => {
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      return res.status(422).json({errors: errors.array()}).end();
    }

    let data = req.body;
    let user = await UserModel.findOne({ email:data.email });
    if (user) {
      // let token = await AuthenticationService.generate(user);
      // EmailService.sendEmail(user, token);
      return res.status(401).json({errors:"El usuario ya existe"}).end();
    } else {
      let newUser = await UserModel.create({
        email: data.email,
        contrasena: bcrypt.hashSync(data.contrasena, 10),
        nombre: data.nombre,
        apellidos: data.apellidos,
        pais: data.pais,
        telefono: data.telefono,
        codigo_postal: data.codigo_postal,
        role: 'customer',
      });
      // let token = await AuthenticationService.generate(newUser);
      // console.log('Created a new user: ', newUser);
      // EmailService.sendEmail(newUser, token);
      res.status(200).end();
    }
  });

  app.post('/v1/login', [
    body('email').isEmail().normalizeEmail(),
    body('contrasena').isLength({min:6}),
  ], async (req, res) => {
    let errors = validationResult(req);
    if(!errors.isEmpty()){
      return res.status(422).json({errors: errors.array()}).end();
    }

    let data = req.body;
    let user = await UserModel.findOne({ email:data.email }).select('+contrasena');
    // console.log("d:",data, " u:",user);
    if (user && bcrypt.compareSync(data.contrasena, user.contrasena)) {
      let token = await AuthenticationService.generate(user);
      res.send({token:token}).status(200).end();
    }else{
      return res.status(401).end();
    }
    return res.status(500).end();
  });

}
