import mongoose, { Schema } from 'mongoose';

export const UserSchema = new Schema({
  nombre:String,
  apellidos:String,
  email:String,
  contrasena:{type:String, select: false},
  pais:String,
  telefono:String,
  codigo_postal:String,
  role:String,
});

export const UserModel = mongoose.model('User', UserSchema);
