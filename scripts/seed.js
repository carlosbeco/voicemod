import mongoose from 'mongoose';
import { UserModel } from '../src/models/User';
const bcrypt = require('bcrypt');

require('dotenv').config();

mongoose.connect(process.env.DB_URL);
const db = mongoose.connection;

db.on('error', (error) => {
  console.error(error);
});

db.once('open', () => {
  console.log('Database connection is open!');
  const fs = require('fs');
  let rawdata = fs.readFileSync('./scripts/mockUsers.json');
  let users = JSON.parse(rawdata);
  users.map((e)=>{
    e.contrasena=bcrypt.hashSync(e.contrasena, 10);
    return e;
  });
  UserModel.insertMany(users, (error) => {
    if (error) {
      console.error(error);
    }
  });
  console.log('Data insertion end!')
});
