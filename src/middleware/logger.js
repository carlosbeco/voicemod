export default (req, res, next) => {
  console.log(
    '=> ',
    req.method,
    req.originalUrl,
    ' || ',
    'User:',
    typeof req.user === 'object',
    ' is Admin: ',
    req.isAdmin
  );
  next();
};
