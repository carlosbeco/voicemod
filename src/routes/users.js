import { UserModel } from '../models/User';
import bodyParser from 'body-parser';
const { check, validationResult, body } = require('express-validator');
const bcrypt = require('bcrypt');

export default (app) => {
  app.get('/v1/users', async (req, res) => {
    //Only admins can list users
    if (!req.user || !req.isAdmin) {
      return res.status(403).end();
    }
    let users = await UserModel.find() || [];
    res.send(users);
  });

  app.get('/v1/user/:id', async (req, res) => {
    //Must be authenticated and be an admin, or in it's own user
    if (!req.user || (!req.isAdmin && req.user.id!==req.params.id)) {
      return res.status(403).end();
    }
    const user = await UserModel.findById(req.params.id);
    if (user) {
      res.send(user);
    } else {
      res.status(404).end();
    }
  });

  //An user must register, not only POST data, so POST is on auth.js

  app.put('/v1/user/:id', [
    body('email').isEmail().normalizeEmail(),
    body('contrasena').isLength({min:6}),
    body('nueva_contrasena').isLength({min:6}),
    body('nombre').not().isEmpty().trim().escape(),
    body('apellidos').not().isEmpty().trim().escape(),
    body('pais').not().isEmpty().trim().escape(),
    body('telefono').not().isEmpty().trim().escape(),
    body('codigo_postal').not().isEmpty().isLength({min:5, max:5}).trim().escape(),
  ], async (req, res) => {
    //Must be authenticated and be an admin, or in it's own user
    if (!req.user || (!req.isAdmin && req.user.id!==req.params.id)) {
      return res.status(403).end();
    }
    const user = await UserModel.findById(req.params.id).select('+contrasena');
    if(user){
      let data = req.body;
      if(data.contrasena && data.nueva_contrasena){
        if(bcrypt.compareSync(data.contrasena, user.contrasena)){
          data.contrasena=bcrypt.hashSync(data.nueva_contrasena, 10);
        }else{
          res.status(400).end();
        }
      }else{
        body.contrasena=user.contrasena;
      }
      delete body.nueva_contrasena;

      if(!req.isAdmin){ data.roles=null; } //Only admins edit roles  
      user.overwrite(data);
      user.save();
      res.status(200).end();
    }else{
      res.status(404).end();
    }
  });

  app.delete('/v1/user/:id', async (req, res) => {
    //Must be authenticated and be an admin, or in it's own user
    if (!req.user || (!req.isAdmin && req.user.id!==req.params.id)) {
      return res.status(403).end();
    }
    await UserModel.deleteOne({_id:req.params.id}).then(d=>{
      res.status(200).end();
    }).catch(e=>{
      res.status(404).end();
    });
  });
}
